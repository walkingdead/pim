package com.zamro.pim.helper;

import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.mapper.CategoryMapper;
import com.zamro.pim.mapper.ProductMapper;
import com.zamro.pim.model.Category;
import com.zamro.pim.model.Product;
import java.math.BigDecimal;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TestHelper {

    public static final String SINGLE_PRODUCT_ID = "A263";
    public static final String SINGLE_PRODUCT_NAME = "Parketschuurschijf 406x12 - Beartex";
    public static final String SINGLE_PRODUCT_DESCRIPTION = "Parketschuurschijf 406x12 - Beartex";
    public static final Double SINGLE_PRODUCT_MIN_ORDER_QUANTITY = 10.0;
    public static final String SINGLE_PRODUCT_UNIT_OF_MEASURE = "PCE";
    public static final Long SINGLE_PRODUCT_CATEGORY_ID = 86978L;
    public static final String SINGLE_PRODUCT_CATEGORY_NAME = "Decoupeerzagen en recipro zaagbladen";
    public static final BigDecimal SINGLE_PRODUCT_PURCHASE_PRICE = (BigDecimal.valueOf(74.19));
    public static final Byte SINGLE_PRODUCT_AVAILABLE = Byte.valueOf("1");

    public static final String VALID_CATEGORY_CSV = String.format("CategoryID,Name\n" +
        "%d,%s", SINGLE_PRODUCT_CATEGORY_ID, SINGLE_PRODUCT_CATEGORY_NAME);
    public static final String INVALID_CATEGORY_CSV = "CategoryIDName\n86978:Decoupeerzagen en recipro zaagbladen";

    public static final String VALID_PRODUCT_CSV =
        String.format("ZamroID,Name,Description,MinOrderQuantity,UnitOfMeasure,CategoryID,PurchasePrice,Available\n"
                + "%s,%s,%s,%f,%s,%d,%f,%d", SINGLE_PRODUCT_ID, SINGLE_PRODUCT_NAME, SINGLE_PRODUCT_DESCRIPTION,
            SINGLE_PRODUCT_MIN_ORDER_QUANTITY, SINGLE_PRODUCT_UNIT_OF_MEASURE, SINGLE_PRODUCT_CATEGORY_ID,
            SINGLE_PRODUCT_PURCHASE_PRICE, SINGLE_PRODUCT_AVAILABLE);

    public static final String INVALID_PRODUCT_CSV =
        "ZamroID,Name,Description,MinOrderQuantity,UnitOfMeasure,CategoryID,PurchasePrice,Available\n"
            + "74C99,Kruipolie Omnigliss Molykote spray 400ml,Smeermiddelen MOLYKOTE:1PCE86978L\t34.18,0";

    public Product generateSingleProduct() {
        return Product.builder()
            .id(SINGLE_PRODUCT_ID)
            .name(SINGLE_PRODUCT_NAME)
            .description(SINGLE_PRODUCT_DESCRIPTION)
            .minOrderQuantity(SINGLE_PRODUCT_MIN_ORDER_QUANTITY)
            .unitOfMeasure(SINGLE_PRODUCT_UNIT_OF_MEASURE)
            .category(generateSingleCategory())
            .purchasePrice(SINGLE_PRODUCT_PURCHASE_PRICE)
            .available(SINGLE_PRODUCT_AVAILABLE)
            .build();
    }

    public Category generateSingleCategory() {
        return Category.builder()
            .id(SINGLE_PRODUCT_CATEGORY_ID)
            .name(SINGLE_PRODUCT_CATEGORY_NAME)
            .build();
    }

    public ProductDto generateSingleProductDto() {
        Product product = generateSingleProduct();
        return ProductMapper.toProductDto(product);
    }

    public CategoryDto generateSingleCategoryDto() {
        return CategoryMapper.toCategoryDto(generateSingleCategory());
    }
}