package com.zamro.pim.it.service.facade;

import static com.zamro.pim.helper.TestHelper.SINGLE_PRODUCT_CATEGORY_ID;
import static com.zamro.pim.helper.TestHelper.SINGLE_PRODUCT_CATEGORY_NAME;
import static com.zamro.pim.helper.TestHelper.VALID_CATEGORY_CSV;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;

import com.zamro.pim.common.annotation.IntegrationTestRunner;
import com.zamro.pim.model.Category;
import com.zamro.pim.repository.CategoryRepository;
import com.zamro.pim.service.facade.CategoryFacadeServiceImpl;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@IntegrationTestRunner
public class CategoryFacadeServiceTestIT {

    @Autowired
    private CategoryFacadeServiceImpl categoryFacadeService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void testImportDataSuccess() {
        MockMultipartFile csvFile = new MockMultipartFile("data", "category.csv", "text/csv",
            VALID_CATEGORY_CSV.getBytes());
        categoryFacadeService.importData(csvFile);

        Optional<Category> categoryOptional = categoryRepository.findById(SINGLE_PRODUCT_CATEGORY_ID);

        assertTrue(categoryOptional.isPresent());

        Category category = categoryOptional.get();

        assertThat("Category zamroId", category.getId(), is(SINGLE_PRODUCT_CATEGORY_ID));
        assertThat("Category name", category.getName(), is(SINGLE_PRODUCT_CATEGORY_NAME));
    }
}
