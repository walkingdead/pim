package com.zamro.pim.it.service.facade;

import static com.zamro.pim.helper.TestHelper.SINGLE_PRODUCT_ID;
import static com.zamro.pim.helper.TestHelper.SINGLE_PRODUCT_NAME;
import static com.zamro.pim.helper.TestHelper.SINGLE_PRODUCT_UNIT_OF_MEASURE;
import static com.zamro.pim.helper.TestHelper.VALID_PRODUCT_CSV;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;

import com.zamro.pim.common.annotation.IntegrationTestRunner;
import com.zamro.pim.helper.TestHelper;
import com.zamro.pim.model.Product;
import com.zamro.pim.repository.CategoryRepository;
import com.zamro.pim.repository.ProductRepository;
import com.zamro.pim.service.facade.ProductFacadeServiceImpl;
import java.util.Optional;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@IntegrationTestRunner
public class ProductFacadeServiceTestIT {

    @Autowired
    private ProductFacadeServiceImpl productFacadeService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testImportDataSuccess() {
        MockMultipartFile csvFile = new MockMultipartFile("data", "product.csv", "text/csv",
            VALID_PRODUCT_CSV.getBytes());

        categoryRepository.save(TestHelper.generateSingleCategory());

        productFacadeService.importData(csvFile);

        Optional<Product> productOptional = productRepository.findById(SINGLE_PRODUCT_ID);

        assertTrue(productOptional.isPresent());

        Product product = productOptional.get();

        assertThat("Product name", product.getName(), is(SINGLE_PRODUCT_NAME));
        assertThat("Product unit of measure", product.getUnitOfMeasure(), is(SINGLE_PRODUCT_UNIT_OF_MEASURE));
    }
}
