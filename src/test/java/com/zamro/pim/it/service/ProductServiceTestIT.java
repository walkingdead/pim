package com.zamro.pim.it.service;

import static java.util.Collections.singletonList;
import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;

import com.zamro.pim.common.annotation.IntegrationTestRunner;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.exception.CategoryNotFoundException;
import com.zamro.pim.exception.ProductNotFoundException;
import com.zamro.pim.helper.TestHelper;
import com.zamro.pim.model.Product;
import com.zamro.pim.repository.CategoryRepository;
import com.zamro.pim.repository.ProductRepository;
import com.zamro.pim.service.ProductService;
import java.util.Optional;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@IntegrationTestRunner
public class ProductServiceTestIT {


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductService productService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testGetById() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        productRepository.saveAndFlush(TestHelper.generateSingleProduct());
        Product product = productService.getById(TestHelper.SINGLE_PRODUCT_ID);
        assertThat("Product found", product, notNullValue());
        assertThat("Product name", product.getName(), is(TestHelper.SINGLE_PRODUCT_NAME));
        assertThat("Product min order quantity", product.getMinOrderQuantity(),
            is(TestHelper.SINGLE_PRODUCT_MIN_ORDER_QUANTITY));
    }

    @Test
    public void testGetByIdNotFound() {
        String nonExistingProductId = "ABCDE";

        expectedException.expect(ProductNotFoundException.class);
        expectedException.expectMessage(String.format("Product with zamroId [%s] not found", nonExistingProductId));

        productService.getById(nonExistingProductId);
    }

    @Test
    public void testDeleteById() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        productRepository.saveAndFlush(TestHelper.generateSingleProduct());

        Optional<Product> productOptional = productRepository.findById(TestHelper.SINGLE_PRODUCT_ID);
        assertTrue(productOptional.isPresent());

        productService.deleteById(TestHelper.SINGLE_PRODUCT_ID);

        Optional<Product> productOptionalDeleted = productRepository.findById(TestHelper.SINGLE_PRODUCT_ID);
        assertFalse(productOptionalDeleted.isPresent());
    }

    @Test
    public void testSaveProductSuccess() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        ProductDto productDto = TestHelper.generateSingleProductDto();

        Product saveProduct = productService.save(productDto);

        assertThat("Product name", saveProduct.getName(), is(TestHelper.SINGLE_PRODUCT_NAME));
        assertThat("Product name", saveProduct.getPurchasePrice(),
            comparesEqualTo(TestHelper.SINGLE_PRODUCT_PURCHASE_PRICE));
    }

    @Test
    public void testSaveProductCategoryNotFound() {
        ProductDto productDto = TestHelper.generateSingleProductDto();

        expectedException.expect(CategoryNotFoundException.class);
        expectedException.expectMessage(String.format("Category with zamroId [%d] not found", productDto.getCategoryId()));

        productService.save(productDto);
    }

    @Test
    public void testUpdateProductSuccess() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        productRepository.saveAndFlush(TestHelper.generateSingleProduct());

        ProductDto productDto = TestHelper.generateSingleProductDto();
        productDto.setName("New name");

        productService.update(productDto, productDto.getZamroId());

        Optional<Product> productOptional = productRepository.findById(productDto.getZamroId());

        assertTrue(productOptional.isPresent());

        Product product = productOptional.get();

        assertThat("Product name", product.getName(), is("New name"));
    }

    @Test
    public void testUpdateProductCategoryNotFound() {
        ProductDto productDto = TestHelper.generateSingleProductDto();

        expectedException.expect(CategoryNotFoundException.class);
        expectedException.expectMessage(String.format("Category with zamroId [%d] not found", productDto.getCategoryId()));

        productService.update(productDto, productDto.getZamroId());
    }

    @Test
    public void testSaveAllProductSuccess() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        ProductDto productDto = TestHelper.generateSingleProductDto();

        productService.saveAll(singletonList(productDto));

        Optional<Product> productOptional = productRepository.findById(productDto.getZamroId());


        assertTrue(productOptional.isPresent());

        Product saveProduct = productOptional.get();

        assertThat("Product name", saveProduct.getName(), is(TestHelper.SINGLE_PRODUCT_NAME));
        assertThat("Product name", saveProduct.getPurchasePrice(),
            comparesEqualTo(TestHelper.SINGLE_PRODUCT_PURCHASE_PRICE));
    }

    @Test
    public void testSaveAllProductCategoryNotFound() {
        ProductDto productDto = TestHelper.generateSingleProductDto();

        expectedException.expect(CategoryNotFoundException.class);
        expectedException.expectMessage(String.format("Category with zamroId [%d] not found", productDto.getCategoryId()));

        productService.saveAll(singletonList(productDto));
    }
}
