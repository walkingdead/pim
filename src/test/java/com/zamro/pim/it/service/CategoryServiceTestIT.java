package com.zamro.pim.it.service;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;

import com.zamro.pim.common.annotation.IntegrationTestRunner;
import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.exception.CategoryNotFoundException;
import com.zamro.pim.helper.TestHelper;
import com.zamro.pim.model.Category;
import com.zamro.pim.repository.CategoryRepository;
import com.zamro.pim.service.CategoryService;
import java.util.Optional;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@IntegrationTestRunner
public class CategoryServiceTestIT {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryService categoryService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testGetById() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());

        Category category = categoryService.getById(TestHelper.SINGLE_PRODUCT_CATEGORY_ID);

        assertThat("Category found", category, notNullValue());
        assertThat("Category name", category.getName(), is(TestHelper.SINGLE_PRODUCT_CATEGORY_NAME));
    }

    @Test
    public void testGetByIdNotFound() {
        Long nonExistingProductId = -1L;

        expectedException.expect(CategoryNotFoundException.class);
        expectedException.expectMessage(String.format("Category with zamroId [%s] not found", nonExistingProductId));

        categoryService.getById(nonExistingProductId);
    }

    @Test
    public void testSaveAllCategorySuccess() {
        CategoryDto categoryDto = TestHelper.generateSingleCategoryDto();
        categoryService.saveAll(singletonList(categoryDto));

        Optional<Category> productOptional = categoryRepository.findById(categoryDto.getCategoryId());


        assertTrue(productOptional.isPresent());

        Category saveCategory = productOptional.get();

        assertThat("Category name", saveCategory.getName(), is(TestHelper.SINGLE_PRODUCT_CATEGORY_NAME));
    }
}
