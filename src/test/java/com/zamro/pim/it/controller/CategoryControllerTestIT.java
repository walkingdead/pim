package com.zamro.pim.it.controller;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import com.zamro.pim.common.annotation.FunctionalTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class CategoryControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${local.server.port}")
    private String port;


    @Test
    public void testUploadCategory() {

        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("file", new ClassPathResource("CategoryDataMin.csv"));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity httpEntity = new HttpEntity<>(parameters, headers);

        ResponseEntity response = restTemplate.exchange(
            "http://localhost:{port}/category/upload",
            HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
            }, port);

        assertThat("Response status should be ok", response.getStatusCode(), is(HttpStatus.OK));
    }
}