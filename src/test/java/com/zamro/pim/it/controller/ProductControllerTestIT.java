package com.zamro.pim.it.controller;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zamro.pim.common.annotation.FunctionalTestRunner;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.helper.TestHelper;
import com.zamro.pim.model.Product;
import com.zamro.pim.repository.CategoryRepository;
import com.zamro.pim.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class ProductControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${local.server.port}")
    private String port;

    @Test
    public void testGetById() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        Product product = productRepository.saveAndFlush(TestHelper.generateSingleProduct());

        ResponseEntity<ProductDto> response = restTemplate.exchange(
            "http://localhost:{port}/product/{zamroId}",
            HttpMethod.GET, null, new ParameterizedTypeReference<ProductDto>() {
            }, port, product.getId());

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        ProductDto productDto = response.getBody();

        assertThat("Product found", productDto, notNullValue());
        assertThat("Product name ", productDto.getName(), is(TestHelper.SINGLE_PRODUCT_NAME));
        assertThat("Product available", productDto.getAvailable(), is(TestHelper.SINGLE_PRODUCT_AVAILABLE));
    }

    @Test
    public void testDeleteById() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        Product product = productRepository.saveAndFlush(TestHelper.generateSingleProduct());

        ResponseEntity response = restTemplate.exchange(
            "http://localhost:{port}/product/{zamroId}",
            HttpMethod.DELETE, null, new ParameterizedTypeReference<Void>() {
            }, port, product.getId());

        assertThat("Response status should be ok", response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void testSaveProduct() throws Exception {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        ProductDto productDto = TestHelper.generateSingleProductDto();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> payload = new HttpEntity<>(objectMapper.writeValueAsString(productDto), headers);

        ResponseEntity response = restTemplate.exchange(
            "http://localhost:{port}/product",
            HttpMethod.POST, payload, new ParameterizedTypeReference<Void>() {
            }, port);

        assertThat("Response status should be created", response.getStatusCode(), is(HttpStatus.CREATED));
        assertThat("Product location", response.getHeaders().get("location").get(0),
            is("http://localhost:8080/product/" + productDto.getZamroId()));
    }

    @Test
    public void testUpdateProduct() throws Exception {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        productRepository.saveAndFlush(TestHelper.generateSingleProduct());

        ProductDto productDto = TestHelper.generateSingleProductDto();
        productDto.setDescription("New Description");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> payload = new HttpEntity<>(objectMapper.writeValueAsString(productDto), headers);

        ResponseEntity response = restTemplate.exchange(
            "http://localhost:{port}/product/{zamroId}",
            HttpMethod.PUT, payload, new ParameterizedTypeReference<Void>() {
            }, port, productDto.getZamroId());

        assertThat("Response status should be no content", response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    @Test
    public void testUploadProduct() {
        categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());

        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("file", new ClassPathResource("ProductDataMin.csv"));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity httpEntity = new HttpEntity<>(parameters, headers);

        ResponseEntity response = restTemplate.exchange(
            "http://localhost:{port}/product/upload",
            HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Void>() {
            }, port);

        assertThat("Response status should be ok", response.getStatusCode(), is(HttpStatus.OK));
    }
}