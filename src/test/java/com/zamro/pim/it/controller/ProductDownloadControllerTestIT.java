package com.zamro.pim.it.controller;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zamro.pim.common.annotation.FunctionalTestRunner;
import com.zamro.pim.dto.CategoryIdsRequest;
import com.zamro.pim.helper.TestHelper;
import com.zamro.pim.model.Category;
import com.zamro.pim.repository.CategoryRepository;
import com.zamro.pim.repository.ProductRepository;
import java.util.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class ProductDownloadControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${local.server.port}")
    private String port;

    private final String csvResponse = "AVAILABLE,CATEGORYID,DESCRIPTION,MINORDERQUANTITY,NAME,PURCHASEPRICE,UNITOFMEASURE,ZAMROID\n"
        + "1,86978,Parketschuurschijf 406x12 - Beartex,10.0,Parketschuurschijf 406x12 - Beartex,74.19,PCE,A263\n";

    @Test
    public void testDownloadByCategoryId() throws Exception {
        Category category = categoryRepository.saveAndFlush(TestHelper.generateSingleCategory());
        productRepository.saveAndFlush(TestHelper.generateSingleProduct());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> payload = new HttpEntity<>(objectMapper.writeValueAsString(
            CategoryIdsRequest.builder().categoryIds(Collections.singletonList(category.getId())).build())
            , headers);


        ResponseEntity<String> response = restTemplate.exchange(
            "http://localhost:{port}/product/download.csv",
            HttpMethod.POST, payload, new ParameterizedTypeReference<String>() {
            }, port);

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

       String productCsv = response.getBody();

        assertThat("Product csv", productCsv, is(csvResponse));
    }
}