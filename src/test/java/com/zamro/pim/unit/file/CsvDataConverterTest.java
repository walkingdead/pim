package com.zamro.pim.unit.file;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import com.opencsv.bean.CsvBindByName;
import com.zamro.pim.file.CsvDataConverter;
import com.zamro.pim.file.FileDataConverter;
import com.zamro.pim.file.MinRequiredFields;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import lombok.Data;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CsvDataConverterTest {

    private final static String VALID_CSV_STRING = "Id,Name\n1,Peter";
    private final static String INVALID_CSV_STRING = "IdName\t1,Peter";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Data
    @MinRequiredFields(count = 2)
    public static class Demo{
        @CsvBindByName(column = "Id", required = true)
        private int id;
        @CsvBindByName(column = "Name", required = true)
        private String name;
    }

    private FileDataConverter<Demo> fileDataConverter = new CsvDataConverter<>();

    @Test
    public void testConvertCsv() throws IOException {
        InputStream stream = new ByteArrayInputStream(VALID_CSV_STRING.getBytes(StandardCharsets.UTF_8));
        List<Demo> demos = fileDataConverter.convert(Demo.class, stream);

        assertThat("Demos size is 1", demos, hasSize(1));

        Demo demo = demos.get(0);

        assertThat("Demos zamroId", demo.getId(), is(1));
        assertThat("Demos name", demo.getName(), is("Peter"));
    }

    @Test
    public void testConvertCsvInvalidFile() throws IOException {
        InputStream stream = new ByteArrayInputStream(INVALID_CSV_STRING.getBytes(StandardCharsets.UTF_8));

        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Error capturing CSV header!");

        List<Demo> demos = fileDataConverter.convert(Demo.class, stream);
    }
}
