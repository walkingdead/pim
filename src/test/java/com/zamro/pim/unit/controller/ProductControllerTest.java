package com.zamro.pim.unit.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.exception.ProductNotFoundException;
import com.zamro.pim.helper.TestHelper;
import com.zamro.pim.rest.ProductController;
import com.zamro.pim.service.ProductService;
import com.zamro.pim.service.facade.ProductFacadeServiceImpl;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    private static final String ROOT_PATH = "/product";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProductService productService;

    @MockBean
    private ProductFacadeServiceImpl productFacadeService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .build();
    }

    @Test
    public void testGetProductById() throws Exception {
        when(productService.getById(TestHelper.SINGLE_PRODUCT_ID)).thenReturn(TestHelper.generateSingleProduct());
        mockMvc.perform(
            get("{root}/{zamroId}", ROOT_PATH, TestHelper.SINGLE_PRODUCT_ID))
            .andExpect(status().isOk())
            .andExpect(content().string("{\"zamroId\":\"A263\",\"name\":\"Parketschuurschijf 406x12 - Beartex\""
                + ",\"description\":\"Parketschuurschijf 406x12 - Beartex\",\"minOrderQuantity\":10.0,"
                + "\"unitOfMeasure\":\"PCE\",\"categoryId\":86978,\"purchasePrice\":74.19,\"available\":1}"));
    }

    @Test
    public void testGetProductByIdNotFound() throws Exception {
        String nonExistingProductId = UUID.randomUUID().toString();
        String errorMessage = String.format("Product with zamroId [%s] not found", nonExistingProductId);
        when(productService.getById(nonExistingProductId)).thenThrow(new ProductNotFoundException(errorMessage));
        mockMvc.perform(
            get("{root}/{zamroId}", ROOT_PATH, nonExistingProductId))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message").value(errorMessage));
    }

    @Test
    public void testSaveProductSuccess() throws Exception {
        ProductDto productDto = TestHelper.generateSingleProductDto();
        when(productService.save(productDto)).thenReturn(TestHelper.generateSingleProduct());
        mockMvc.perform(
            post(ROOT_PATH)
                .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productDto)))
            .andExpect(status().isCreated())
            .andExpect(header().string("location", "http://localhost/product/" + TestHelper.SINGLE_PRODUCT_ID));
    }

    @Test
    public void testSaveProductMissedName() throws Exception {
        ProductDto productDto = TestHelper.generateSingleProductDto();
        productDto.setName(null);
        mockMvc.perform(
            post(ROOT_PATH)
                .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productDto)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("Parameter productDto [Name is required]"));
    }

    @Test
    public void testSaveProductMissedDescription() throws Exception {
        ProductDto productDto = TestHelper.generateSingleProductDto();
        productDto.setDescription(null);
        mockMvc.perform(
            post(ROOT_PATH)
                .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productDto)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("Parameter productDto [Description is required]"));
    }

    @Test
    public void testUpdateProductSuccess() throws Exception {
        ProductDto productDto = TestHelper.generateSingleProductDto();
        mockMvc.perform(
            put("{root}/{zamroId}", ROOT_PATH, productDto.getZamroId())
                .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productDto)))
            .andExpect(status().isNoContent());
    }

    @Test
    public void testUpdateProductNotFound() throws Exception {
        ProductDto productDto = TestHelper.generateSingleProductDto();
        String nonExistingProductId = UUID.randomUUID().toString();
        String errorMessage = String.format("Product with zamroId [%s] not found", nonExistingProductId);
        doAnswer(invocation -> {
            throw new ProductNotFoundException(errorMessage);
        }).when(productService).update(productDto, productDto.getZamroId());

        mockMvc.perform(
            put("{root}/{zamroId}", ROOT_PATH, productDto.getZamroId())
                .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productDto)))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message").value(errorMessage));
    }

    @Test
    public void testUpdateProductMissedPrice() throws Exception {
        ProductDto productDto = TestHelper.generateSingleProductDto();
        productDto.setPurchasePrice(null);
        mockMvc.perform(
            put("{root}/{zamroId}", ROOT_PATH, productDto.getZamroId())
                .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productDto)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("Parameter productDto [Purchase Price is required]"));
    }

    @Test
    public void testDeleteProduct() throws Exception {
        ProductDto productDto = TestHelper.generateSingleProductDto();
        mockMvc.perform(
            delete("{root}/{zamroId}", ROOT_PATH, productDto.getZamroId()))
            .andExpect(status().isOk());
    }

    @Test
    public void testUploadProductSuccess() throws Exception {
        mockMvc.perform(
            multipart("{root}/upload", ROOT_PATH)
                .file("file", "some csv".getBytes())
                .contentType(MediaType.MULTIPART_FORM_DATA))
            .andExpect(status().isOk());
    }

    @Test
    public void testUploadProductFailure() throws Exception {
        String errorMessage = "Failed to handle file product.csv";
        doAnswer(invocation -> {
            throw new FileDataException(errorMessage);
        }).when(productFacadeService).importData(any());
        mockMvc.perform(
            multipart("{root}/upload", ROOT_PATH)
                .file("file", "some csv".getBytes())
                .contentType(MediaType.MULTIPART_FORM_DATA))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value(errorMessage));
    }
}
