package com.zamro.pim.unit.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.rest.CategoryController;
import com.zamro.pim.service.facade.CategoryFacadeServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerTest {

    private static final String ROOT_PATH = "/category";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private CategoryFacadeServiceImpl categoryFacadeService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .build();
    }

    @Test
    public void testUploadProductSuccess() throws Exception {
        mockMvc.perform(
            multipart("{root}/upload", ROOT_PATH)
                .file("file", "some csv".getBytes())
                .contentType(MediaType.MULTIPART_FORM_DATA))
            .andExpect(status().isOk());
    }

    @Test
    public void testUploadProductFailure() throws Exception {
        String errorMessage = "Failed to handle file category.csv";
        doAnswer(invocation -> {
            throw new FileDataException(errorMessage);
        }).when(categoryFacadeService).importData(any());
        mockMvc.perform(
            multipart("{root}/upload", ROOT_PATH)
                .file("file", "some csv".getBytes())
                .contentType(MediaType.MULTIPART_FORM_DATA))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value(errorMessage));
    }
}
