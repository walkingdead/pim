package com.zamro.pim.unit.service;

import static org.mockito.Mockito.when;

import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.file.CsvDataConverter;
import com.zamro.pim.service.FileDataService;
import com.zamro.pim.service.ProductService;
import com.zamro.pim.service.facade.ProductFacadeServiceImpl;
import java.io.IOException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(MockitoJUnitRunner.class)
public class ProductFacadeServiceTest {

    private ProductFacadeServiceImpl productFacadeService;

    @Mock
    private MultipartFile multipartFile;

    @Mock
    private ProductService productService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init(){
        productFacadeService = new ProductFacadeServiceImpl(
            new FileDataService<>(new CsvDataConverter<>()),
            productService
        );
    }

    @Test
    public void testFailedToHandleData() throws IOException {
        String fileName = "product.csv";

        expectedException.expect(FileDataException.class);
        expectedException.expectMessage("Failed to handle file " + fileName);

        when(multipartFile.getInputStream()).thenThrow(new IOException(""));
        when(multipartFile.getName()).thenReturn(fileName);

        productFacadeService.importData(multipartFile);
    }
}
