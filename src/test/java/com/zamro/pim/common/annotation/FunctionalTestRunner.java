package com.zamro.pim.common.annotation;


import com.zamro.pim.common.config.FunctionalTestConfiguration;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@ActiveProfiles("test")
@ContextConfiguration(classes = FunctionalTestConfiguration.class)
public @interface FunctionalTestRunner {
}
