package com.zamro.pim.common.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@Profile("test")
@ComponentScan(basePackages = "com.zamro.pim")
public class IntegrationTestConfiguration {
}
