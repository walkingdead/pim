package com.zamro.pim.mapper;

import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.model.Category;
import lombok.experimental.UtilityClass;

/**
 * Utility class for conversion domain category -> dto category and vice versa
 */
@UtilityClass
public class CategoryMapper {
    
    public static Category toCategory(CategoryDto categoryDto){
        return Category.builder().id(categoryDto.getCategoryId()).name(categoryDto.getName()).build();
    }

    public static CategoryDto toCategoryDto(Category category){
        return CategoryDto.builder().categoryId(category.getId()).name(category.getName()).build();
    }
}
