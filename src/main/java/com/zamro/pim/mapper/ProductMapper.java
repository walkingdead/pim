package com.zamro.pim.mapper;

import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.model.Category;
import com.zamro.pim.model.Product;
import lombok.experimental.UtilityClass;

/**
 * Utility class for conversion domain product -> dto product and vice versa
 */
@UtilityClass
public class ProductMapper {

    public static Product toProduct(ProductDto productDto, Category category) {
        return Product.builder()
            .id(productDto.getZamroId())
            .name(productDto.getName())
            .description(productDto.getDescription())
            .minOrderQuantity(productDto.getMinOrderQuantity())
            .category(category)
            .purchasePrice(productDto.getPurchasePrice())
            .unitOfMeasure(productDto.getUnitOfMeasure())
            .available(productDto.getAvailable())
            .build();
    }

    public static ProductDto toProductDto(Product product) {
        return ProductDto.builder()
            .zamroId(product.getId())
            .name(product.getName())
            .description(product.getDescription())
            .minOrderQuantity(product.getMinOrderQuantity())
            .categoryId(product.getCategory().getId())
            .purchasePrice(product.getPurchasePrice())
            .unitOfMeasure(product.getUnitOfMeasure())
            .available(product.getAvailable())
            .build();
    }

}
