package com.zamro.pim.donwload;

import java.util.Locale;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

/**
 * Creates new Csv view
 */
public class CsvViewResolver implements ViewResolver {

    @Override
    public View resolveViewName(String s, Locale locale) {
        return new CsvView();
    }
}
