package com.zamro.pim.donwload;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Concrete csv view implementation that build csv document.
 * @param <T> generic data type parameter
 */
public class CsvView<T> extends AbstractCsvView<T> {

    /**
     * Build csv document and writes it to response
     *
     * @param model model that contains collection of pojos
     * @param request httpServletRequest
     * @param response httpServletResponse with actual document payload
     * @param <T> pojo type parameter
     * @throws Exception if error occurred during write operation to response
     */
    @Override
    @SuppressWarnings("unchecked")
    protected <T> void buildCsvDocument(Map<String, Object> model, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        response.setHeader("Content-Disposition", "attachment; filename=\"file.csv\"");

        Writer writer = response.getWriter();

        List<T> data = (List<T>) model.get("data");

        StatefulBeanToCsv<T> beanToCsv = new StatefulBeanToCsvBuilder(writer)
            .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
            .build();
        beanToCsv.write(data);
    }
}
