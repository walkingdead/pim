package com.zamro.pim.donwload;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.view.AbstractView;

/**
 * The abstract csv view that contains base logic for document rendering.
 */
public abstract class AbstractCsvView<T> extends AbstractView {

    private static final String CONTENT_TYPE = "text/csv";

    /**
     * This constructor sets the appropriate content type "text/csv".
     */
    public AbstractCsvView() {
        setContentType(CONTENT_TYPE);
    }

    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }

    @Override
    protected final void renderMergedOutputModel(
        Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType(getContentType());
        buildCsvDocument(model, request, response);
    }

    protected abstract <T> void buildCsvDocument(
        Map<String, Object> model, HttpServletRequest request, HttpServletResponse response)
        throws Exception;

}
