package com.zamro.pim.exception;

/**
 * Class used to indicate that error occurred during handling data from/to file.
 */
public class FileDataException extends RuntimeException {

    public FileDataException() {
        super();
    }

    public FileDataException(String message) {
        super(message);
    }

    public FileDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
