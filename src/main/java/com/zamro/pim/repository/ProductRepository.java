package com.zamro.pim.repository;

import com.zamro.pim.model.Product;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Product repository for basic CRUD and search operation
 */
public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findAllByCategoryId(List<Long> categoryIds);
}
