package com.zamro.pim.repository;

import com.zamro.pim.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Category repository for basic CRUD operation
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
