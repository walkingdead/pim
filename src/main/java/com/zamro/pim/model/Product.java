package com.zamro.pim.model;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

/**
 * Contains product details
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
//@BatchSize(size = 100)
public class Product {

    @Id
    private String id;

    @OneToOne
    private Category category;

    private String name;

    private String description;

    private Double minOrderQuantity;

    private String unitOfMeasure;

    private BigDecimal purchasePrice;

    private Byte available;
}
