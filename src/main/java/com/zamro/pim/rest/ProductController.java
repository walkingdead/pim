package com.zamro.pim.rest;

import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.mapper.ProductMapper;
import com.zamro.pim.model.Product;
import com.zamro.pim.service.ProductService;
import com.zamro.pim.service.facade.ProductBatchFacadeServiceImpl;
import com.zamro.pim.service.facade.ProductFacadeServiceImpl;
import java.io.IOException;
import java.net.URI;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * The controller which manages service calls to product backend service.
 */
@RestController
@RequestMapping("product")
@RequiredArgsConstructor
@Validated
public class ProductController {

    private final ProductService productService;
    private final ProductFacadeServiceImpl productFacadeService;
    private final ProductBatchFacadeServiceImpl productBatchFacadeService;

    /**
     * Retrieves product by given ID
     * @param id search product zamroId
     * @return response entity with product dto as a body
     */
    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> getById(@PathVariable("id") String id) {
        Product product = productService.getById(id);

        return ResponseEntity.ok(ProductMapper.toProductDto(product));
    }

    /**
     * Stores product
     * @param productDto request product to be saved
     * @return response entity with status created and location of new resource in 'location' header
     */
    @PostMapping
    public ResponseEntity<Object> saveProduct(@RequestBody @Valid ProductDto productDto) {
        Product product = productService.save(productDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{zamroId}")
            .buildAndExpand(product.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    /**
     * Update existing product
     * @param id update product zamroId
     * @param productDto request product to be updated
     * @return response entity with status no content
     */
    @PutMapping("/{id}")
    public ResponseEntity updateProduct(@PathVariable String id, @RequestBody @Valid ProductDto productDto) {
        productService.update(productDto, id);

        return ResponseEntity.noContent().build();
    }

    /**
     * Removed product
     * @param id delete product zamroId
     * @return response entity with status ok
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") String id) {
        productService.deleteById(id);

        return ResponseEntity.ok().build();
    }

    /**
     * Stores product data from file
     * @param file file with product data
     * @return response entity with status ok
     */
    @PostMapping("/upload")
    public ResponseEntity uploadProduct(@RequestParam("file") MultipartFile file) throws IOException {
        productFacadeService.importData(file);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/upload/batch")
    public ResponseEntity uploadBatchProduct(@RequestParam("file") MultipartFile file) throws IOException {
        productBatchFacadeService.importData(file);
        return ResponseEntity.ok().build();
    }
}
