package com.zamro.pim.rest;

import com.zamro.pim.service.facade.CategoryBatchFacadeServiceImpl;
import com.zamro.pim.service.facade.CategoryFacadeServiceImpl;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * The controller which manages service calls to category backend.
 */
@RestController
@RequestMapping("category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryFacadeServiceImpl categoryFacadeService;
    private final CategoryBatchFacadeServiceImpl categoryBatchFacadeService;


    /**
     * Stores category data from file
     * @param file file with category data
     * @return response entity with status ok
     */
    @PostMapping("/upload")
    public ResponseEntity uploadCategory(@RequestParam("file") MultipartFile file) throws IOException {
        categoryFacadeService.importData(file);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/upload/batch")
    public ResponseEntity uploadBatchCategory(@RequestParam("file") MultipartFile file) throws IOException {
        categoryBatchFacadeService.importData(file);
        return ResponseEntity.ok().build();
    }
}
