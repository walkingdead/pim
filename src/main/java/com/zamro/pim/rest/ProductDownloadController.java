package com.zamro.pim.rest;

import com.zamro.pim.dto.CategoryIdsRequest;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.mapper.ProductMapper;
import com.zamro.pim.service.ProductService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The controller which manages download product in specified file format
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("product")
@Validated
public class ProductDownloadController {

    private final ProductService productService;

    /**
     * Downloads list of product in selected format
     * @param categoryIdsRequest list of category ids
     * @param model model that contains list of products
     * @return data in selected format (csv, pdf, etc)
     */
    @PostMapping(value = "/download")
    public String download(@RequestBody @Valid CategoryIdsRequest categoryIdsRequest, Model model) {
        List<ProductDto> productDtos = productService.getAllByCategoryIds(categoryIdsRequest.getCategoryIds())
            .stream().map(ProductMapper::toProductDto).collect(Collectors.toList());
        model.addAttribute("data", productDtos);
        return "";
    }
}
