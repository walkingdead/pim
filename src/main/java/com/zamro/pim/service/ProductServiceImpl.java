package com.zamro.pim.service;

import static java.util.stream.Collectors.toList;

import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.exception.ProductNotFoundException;
import com.zamro.pim.mapper.ProductMapper;
import com.zamro.pim.model.Category;
import com.zamro.pim.model.Product;
import com.zamro.pim.repository.ProductRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final CategoryService categoryService;

    /**
     * Retrieves product by zamroId. If not found throws Exception {@link ProductNotFoundException}
     * @param id product zamroId
     * @return product data
     */
    @Override
    public Product getById(String id) {
        Optional<Product> productOptional = productRepository.findById(id);
        return productOptional.orElseThrow(() -> new ProductNotFoundException(
            String.format("Product with zamroId [%s] not found", id)));
    }

    /**
     * Stores product. Checks if category present.
     * @param productDto product data
     * @return saved product
     */
    @Override
    public Product save(ProductDto productDto) {
        Category category = categoryService.getById(productDto.getCategoryId());
        Product product = ProductMapper.toProduct(productDto, category);
        return productRepository.save(product);
    }

    /**
     * Retrieves products by category ids.
     * @param categoryIds list of category ids
     * @return list of found products
     */
    @Override
    public List<Product> getAllByCategoryIds(List<Long> categoryIds) {
        return productRepository.findAllByCategoryId(categoryIds);
    }

    /**
     * Stores all products. Checks if category present.
     * @param productDtos list of product data
     */
    @Override
    public void saveAll(List<? extends ProductDto> productDtos) {
        List<Product> products = productDtos.stream().map(
            p -> ProductMapper.toProduct(p, categoryService.getById(p.getCategoryId())))
            .collect(toList());
        productRepository.saveAll(products);
    }

    /**
     * Update existing product. Checks if category and producta are present.
     * @param productDto new product data
     * @param id product zamroId to be updated
     */
    @Override
    public void update(ProductDto productDto, String id) {
        Category category = categoryService.getById(productDto.getCategoryId());
        Product product = ProductMapper.toProduct(productDto, category);
        Product saveProduct = getById(id);
        product.setId(saveProduct.getId());
        productRepository.save(product);
    }

    /**
     * Removed product by given zamroId.
     * @param id product zamroId to be deleted
     */
    @Override
    public void deleteById(String id) {
        productRepository.deleteById(id);
    }
}
