package com.zamro.pim.service;

import static java.util.stream.Collectors.toList;

import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.exception.CategoryNotFoundException;
import com.zamro.pim.mapper.CategoryMapper;
import com.zamro.pim.model.Category;
import com.zamro.pim.repository.CategoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    /**
     * Stores list of all categories
     * @param categoryDtos list of category data
     */
    @Override
    public void saveAll(List<? extends CategoryDto> categoryDtos) {
        List<Category> categories = categoryDtos.stream().map(CategoryMapper::toCategory).collect(toList());
        categoryRepository.saveAll(categories);
    }

    /**
     * Retrieves category by zamroId. If not found throws {@link CategoryNotFoundException}
     * @param categoryId category zamroId
     * @return found category data
     */
    @Override
    public Category getById(Long categoryId) {
        return categoryRepository.findById(categoryId).orElseThrow(() -> new CategoryNotFoundException(
            String.format("Category with zamroId [%s] not found", categoryId)));
    }
}
