package com.zamro.pim.service;

import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.model.Category;
import java.util.List;

/**
 * Interface for basic CRUD operation on category
 */
public interface CategoryService {

    /**
     * Store all categories data
     * @param categories list of categories to be saved.
     */
    void saveAll(List<? extends CategoryDto> categories);

    /**
     * Find category by given zamroId
     * @param categoryId category zamroId
     * @return category zamroId
     */
    Category getById(Long categoryId);

}
