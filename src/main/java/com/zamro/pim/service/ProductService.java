package com.zamro.pim.service;

import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.model.Product;
import java.util.List;

/**
 * Interface for basic CRUD operation on product
 */
public interface ProductService {

    /**
     * Find product by given zamroId
     * @param id product zamroId
     * @return found product
     */
    Product getById(String id);

    /**
     * Stores product data
     * @param productDto product data
     * @return saved product
     */
    Product save(ProductDto productDto);

    /**
     * Retrievess list of all product by given category ids
     * @param categoryIds list of category ids
     * @return list of products
     */
    List<Product> getAllByCategoryIds(List<Long> categoryIds);

    /**
     * Stores all product data
     * @param productDtos list of product data
     */
    void saveAll(List<? extends ProductDto> productDtos);

    /**
     * Updates existing product with new data by given zamroId
     * @param productDto new product data
     * @param id product zamroId to be updated
     */
    void update(ProductDto productDto, String id);

    /**
     * Removed product by given zamroId
     * @param id product zamroId to be deleted
     */
    void deleteById(String id);

}
