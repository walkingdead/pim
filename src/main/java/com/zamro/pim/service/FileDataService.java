package com.zamro.pim.service;

import com.zamro.pim.file.FileDataConverter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Class for converting input stream data to java beans
 * @param <T> target java bean type
 */
public class FileDataService<T> {

    private final FileDataConverter<T> dataConverter;

    public FileDataService(FileDataConverter<T> dataConverter) {
        this.dataConverter = dataConverter;
    }

    /**
     * Converts input stream raw data to list of target java beans
     * @param clazz taget javs bean class
     * @param inputStream input stream with raw data
     * @return list of converted java beans
     * @throws IOException if unable to handle input data
     */
    public List<T> convert(Class<T> clazz, InputStream inputStream) throws IOException {
        return dataConverter.convert(clazz, inputStream);

    }
}
