package com.zamro.pim.service.facade;

import static com.zamro.pim.batch.constants.ColumnNames.PRODUCT_COLUMN_NAMES;

import com.zamro.pim.batch.processor.ProductProcessor;
import com.zamro.pim.batch.reader.ItemReaderFactory;
import com.zamro.pim.batch.BatchFailedException;
import com.zamro.pim.batch.CustomJobBuilder;
import com.zamro.pim.batch.JobParametersDto;
import com.zamro.pim.batch.writer.ProductWriter;
import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.model.Product;
import java.io.IOException;
import java.io.InputStream;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.support.PassThroughItemProcessor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class that handles import raw product data
 */
@Service
@RequiredArgsConstructor
public class ProductBatchFacadeServiceImpl implements ItemFacadeService {

    private final JobLauncher jobLauncher;
    private final CustomJobBuilder customJobBuilder;
    private final ProductWriter productWriter;
    private final ProductProcessor productProcessor;

    /**
     * Converts raw data to {@link CategoryDto} and stores them in database. If failed to handle input data throws
     * {@link FileDataException}
     *
     * @param file file with raw product data
     */
    public void importData(MultipartFile file) {
        InputStream inputStream;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            throw new FileDataException(String.format("Failed to handle file %s. %s", file.getName(), e.getMessage()));
        }

        FlatFileItemReader<ProductDto> itemReader = ItemReaderFactory.csvReader(inputStream, PRODUCT_COLUMN_NAMES,
            ProductDto.class);
        JobParametersDto<ProductDto, ProductDto> jobParametersDto = JobParametersDto.<ProductDto, ProductDto>builder()
            .batchSize(1000)
            .itemReader(itemReader)
            .itemWriter(productWriter)
            .itemProcessor(productProcessor)
            .entityName(Product.class.getSimpleName())
            .build();
        Job productJob = customJobBuilder.buildJob(jobParametersDto);
        try {
            jobLauncher.run(productJob, new JobParameters());
        } catch (Exception e) {
            throw new BatchFailedException("Failed to handle Product batch", e);
        }
    }
}
