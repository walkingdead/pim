package com.zamro.pim.service.facade;

import static com.zamro.pim.batch.constants.ColumnNames.CATEGORY_COLUMN_NAMES;

import com.zamro.pim.batch.reader.ItemReaderFactory;
import com.zamro.pim.batch.BatchFailedException;
import com.zamro.pim.batch.CustomJobBuilder;
import com.zamro.pim.batch.JobParametersDto;
import com.zamro.pim.batch.writer.CategoryWriter;
import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.model.Category;
import java.io.IOException;
import java.io.InputStream;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.support.PassThroughItemProcessor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class that handles import raw category data
 */
@Service
@RequiredArgsConstructor
public class CategoryBatchFacadeServiceImpl implements ItemFacadeService {

    private final JobLauncher jobLauncher;
    private final CustomJobBuilder customJobBuilder;
    private final CategoryWriter categoryWriter;

    /**
     * Converts raw data to {@link CategoryDto} and stores them in database. If failed to handle input data throws
     * {@link FileDataException}
     *
     * @param file file with raw product data
     */
    public void importData(MultipartFile file) {
        InputStream inputStream;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            throw new FileDataException(String.format("Failed to handle file %s. %s", file.getName(), e.getMessage()));
        }

        FlatFileItemReader<CategoryDto> itemReader = ItemReaderFactory.csvReader(inputStream, CATEGORY_COLUMN_NAMES,
            CategoryDto.class);
        JobParametersDto<CategoryDto, CategoryDto> jobParametersDto = JobParametersDto.<CategoryDto, CategoryDto>builder()
            .batchSize(10)
            .itemReader(itemReader)
            .itemWriter(categoryWriter)
            .itemProcessor(new PassThroughItemProcessor<>())
            .entityName(Category.class.getSimpleName())
            .build();
        Job productJob = customJobBuilder.buildJob(jobParametersDto);
        try {
            jobLauncher.run(productJob, new JobParameters());
        } catch (Exception e) {
            throw new BatchFailedException("Failed to handle Category batch", e);
        }
    }
}
