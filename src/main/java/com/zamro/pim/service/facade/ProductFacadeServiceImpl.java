package com.zamro.pim.service.facade;

import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.service.FileDataService;
import com.zamro.pim.service.ProductService;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class that handles import raw product data
 */
@Service
public class ProductFacadeServiceImpl implements ItemFacadeService {

    private final FileDataService<ProductDto> fileDataService;
    private final ProductService productService;

    public ProductFacadeServiceImpl(@Qualifier("productFileDataService") FileDataService<ProductDto> fileDataService,
        ProductService productService) {
        this.fileDataService = fileDataService;
        this.productService = productService;
    }

    /**
     * Converts raw data to {@link ProductDto} and stores them in database.
     * If failed to handle input data throws {@link FileDataException}
     * @param file file with raw product data
     */
    public void importData(MultipartFile file){
        try {
            List<ProductDto> productDtos = fileDataService.convert(ProductDto.class, file.getInputStream());
            productService.saveAll(productDtos);
        } catch (IOException e) {
            throw new FileDataException(String.format("Failed to handle file %s. %s", file.getName(), e.getMessage()));
        }
    }
}
