package com.zamro.pim.service.facade;

import org.springframework.web.multipart.MultipartFile;


public interface ItemFacadeService {

    void importData(MultipartFile file);
}
