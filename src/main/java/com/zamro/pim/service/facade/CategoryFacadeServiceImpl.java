package com.zamro.pim.service.facade;

import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.service.CategoryService;
import com.zamro.pim.service.FileDataService;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class that handles import raw category data
 */
@Service
public class CategoryFacadeServiceImpl implements ItemFacadeService {

    private final FileDataService<CategoryDto> fileDataService;
    private final CategoryService categoryService;

    public CategoryFacadeServiceImpl(@Qualifier("categoryFileDataService") FileDataService<CategoryDto> fileDataService,
        CategoryService categoryService) {
        this.fileDataService = fileDataService;
        this.categoryService = categoryService;
    }

    /**
     * Converts raw data to {@link CategoryDto} and stores them in database.
     * If failed to handle input data throws {@link FileDataException}
     * @param file file with raw product data
     */
    public void importData(MultipartFile file){
        try {
            List<CategoryDto> categoryDtos = fileDataService.convert(CategoryDto.class, file.getInputStream());
            categoryService.saveAll(categoryDtos);
        } catch (IOException e) {
            throw new FileDataException(String.format("Failed to handle file %s. %s", file.getName(), e.getMessage()));
        }
    }
}
