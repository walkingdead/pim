package com.zamro.pim.batch;

    import org.apache.commons.lang3.StringUtils;
    import org.springframework.batch.item.file.separator.SimpleRecordSeparatorPolicy;

public class BlankLineRecordSeparatorPolicy extends SimpleRecordSeparatorPolicy {

    @Override
    public boolean isEndOfRecord(String line) {
        return StringUtils.isNotBlank(line) && super.isEndOfRecord(line);
    }

    @Override
    public String postProcess(String record) {
        return StringUtils.isBlank(record) ? null : super.postProcess(record);
    }
}
