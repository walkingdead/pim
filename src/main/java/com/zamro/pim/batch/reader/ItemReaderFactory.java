package com.zamro.pim.batch.reader;

import com.zamro.pim.batch.BlankLineRecordSeparatorPolicy;
import java.io.InputStream;
import lombok.experimental.UtilityClass;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.InputStreamResource;

@UtilityClass
public class ItemReaderFactory {

    public static <T> FlatFileItemReader<T> csvReader(InputStream inputStream, String[] columns, Class<T> targetClass) {
        FlatFileItemReader<T> reader = new FlatFileItemReader<>();
        reader.setResource(new InputStreamResource(inputStream));
        reader.setLinesToSkip(1);

        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
        delimitedLineTokenizer.setNames(columns);

        BeanWrapperFieldSetMapper<T> beanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper<>();
        beanWrapperFieldSetMapper.setTargetType(targetClass);

        DefaultLineMapper<T> lineMapper = new DefaultLineMapper<>();
        lineMapper.setLineTokenizer(delimitedLineTokenizer);
        lineMapper.setFieldSetMapper(beanWrapperFieldSetMapper);

        reader.setLineMapper(lineMapper);
        reader.setRecordSeparatorPolicy(new BlankLineRecordSeparatorPolicy());
        return reader;
    }
}
