package com.zamro.pim.batch.writer;


import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.service.ProductService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductWriter implements ItemWriter<ProductDto> {

    private final ProductService productService;

    @Override
    public void write(List<? extends ProductDto> items) {
        productService.saveAll(items);
    }
}
