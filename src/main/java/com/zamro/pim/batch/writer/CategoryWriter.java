package com.zamro.pim.batch.writer;


import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.service.CategoryService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CategoryWriter implements ItemWriter<CategoryDto> {

    private final CategoryService categoryService;

    @Override
    public void write(List<? extends CategoryDto> items) {
        categoryService.saveAll(items);
    }
}
