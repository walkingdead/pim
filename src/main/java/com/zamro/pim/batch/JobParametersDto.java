package com.zamro.pim.batch;

import lombok.Builder;
import lombok.Data;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;

@Builder
@Data
public class JobParametersDto<T, U> {
    private ItemReader<T> itemReader;
    private ItemWriter<U> itemWriter;
    private ItemProcessor<T, U> itemProcessor;
    private String entityName;
    private int batchSize;
}
