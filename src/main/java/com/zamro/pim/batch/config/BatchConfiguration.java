package com.zamro.pim.batch.config;

import com.zamro.pim.batch.writer.CategoryWriter;
import com.zamro.pim.batch.writer.ProductWriter;
import com.zamro.pim.service.CategoryService;
import com.zamro.pim.service.ProductService;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Bean
    public ProductWriter productWriter(ProductService productService) {
        return new ProductWriter(productService);
    }

    @Bean
    public CategoryWriter categoryWriter(CategoryService categoryService) {
        return new CategoryWriter(categoryService);
    }
}
