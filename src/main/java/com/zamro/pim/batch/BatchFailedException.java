package com.zamro.pim.batch;

public class BatchFailedException extends RuntimeException {

    public BatchFailedException() {
        super();
    }

    public BatchFailedException(String message) {
        super(message);
    }

    public BatchFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
