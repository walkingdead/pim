package com.zamro.pim.batch.constants;

public class ColumnNames {

    public static final String[] PRODUCT_COLUMN_NAMES =
        new String[]{
            "ZamroID", "Name", "Description", "MinOrderQuantity", "UnitOfMeasure", "CategoryID", "PurchasePrice",
            "Available"
        };

    public static final String[] CATEGORY_COLUMN_NAMES = new String[]{"CategoryID", "Name"};
}
