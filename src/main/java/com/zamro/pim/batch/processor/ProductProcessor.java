package com.zamro.pim.batch.processor;

import com.zamro.pim.dto.ProductDto;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class ProductProcessor implements ItemProcessor<ProductDto, ProductDto> {

    private final Validator validator;

    @Override
    public ProductDto process(ProductDto item) throws Exception {
        Errors errors = new BeanPropertyBindingResult(item, ProductDto.class.getSimpleName());
        validator.validate(item, errors);
        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(e -> System.out.println(e.getObjectName() + " - " + e.getDefaultMessage()));
            return null;
        }
        return item;
    }
}
