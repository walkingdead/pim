package com.zamro.pim.batch;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class CustomJobBuilder {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    public <T, U> Job buildJob(JobParametersDto<T, U> jopParametersDto) {
        return csvFileToDatabaseJob(jopParametersDto);
    }

    private <T, U> Step csvFileToDatabaseStep(JobParametersDto<T, U> jopParametersDto) {
        return stepBuilderFactory.get(String.format("csvFileTo%sDatabaseStep", jopParametersDto.getEntityName()))
            .<T, U>chunk(jopParametersDto.getBatchSize())
            .reader(jopParametersDto.getItemReader())
            .processor(jopParametersDto.getItemProcessor())
            .writer(jopParametersDto.getItemWriter())
            .build();
    }

    private <T, U> Job csvFileToDatabaseJob(JobParametersDto<T, U> jopParametersDto) {
        return jobBuilderFactory.get(String.format("csvFileTo%sDatabaseJob", jopParametersDto.getEntityName()))
            .incrementer(new RunIdIncrementer())
            .flow(csvFileToDatabaseStep(jopParametersDto))
            .end()
            .build();
    }
}
