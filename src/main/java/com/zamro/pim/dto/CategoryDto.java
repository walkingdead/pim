package com.zamro.pim.dto;

import com.opencsv.bean.CsvBindByName;
import com.zamro.pim.file.MinRequiredFields;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class that is used as response payload for category and mapping for open-csv
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@MinRequiredFields(count = 2)
public class CategoryDto {

    @CsvBindByName(column = "CategoryID")
    private Long categoryId;

    @CsvBindByName(column = "Name")
    private String name;


}
