package com.zamro.pim.dto;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class that is used as category ids request.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryIdsRequest {

    @NotEmpty(message = "categoryIds are required")
    private List<Long> categoryIds;
}
