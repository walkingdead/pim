package com.zamro.pim.dto;

import com.opencsv.bean.CsvBindByName;
import com.zamro.pim.file.MinRequiredFields;
import java.math.BigDecimal;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class that is used as response payload for product and mapping for open-csv
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@MinRequiredFields(count = 5)
public class ProductDto {

    @CsvBindByName(column = "ZamroID", required = true)
    private String zamroId;

    @CsvBindByName(column = "Name", required = true)
    @NotEmpty(message = "Name is required")
    private String name;

    @CsvBindByName(column = "Description")
    @NotEmpty(message = "Description is required")
    private String description;

    @CsvBindByName(column = "MinOrderQuantity")
    @NotNull(message = "Min Order Quantity is required")
    private Double minOrderQuantity;

    @CsvBindByName(column = "UnitOfMeasure")
    @NotEmpty(message = "Unit Of Measure is required")
    private String unitOfMeasure;

    @CsvBindByName(column = "CategoryID", required = true)
    @NotNull(message = "Category Id is required")
    private Long categoryId;

    @CsvBindByName(column = "PurchasePrice", required = true)
    @NotNull(message = "Purchase Price is required")
    private BigDecimal purchasePrice;

    @CsvBindByName(column = "Available", required = true)
    @NotNull(message = "Available is required")
    private Byte available;
}
