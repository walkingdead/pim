package com.zamro.pim;

import com.zamro.pim.dto.CategoryDto;
import com.zamro.pim.dto.ProductDto;
import com.zamro.pim.file.CsvDataConverter;
import com.zamro.pim.service.FileDataService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Application configuration
 */
@Configuration
public class PimConfiguration {

    @Bean
    public FileDataService<CategoryDto> categoryFileDataService() {
        return new FileDataService<>(new CsvDataConverter<>());
    }

    @Bean
    public FileDataService<ProductDto> productFileDataService() {
        return new FileDataService<>(new CsvDataConverter<>());
    }
}
