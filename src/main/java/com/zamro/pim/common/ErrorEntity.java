package com.zamro.pim.common;

import lombok.Data;

/**
 * Class that keep simple error description
 */
@Data
public class ErrorEntity {

    private final String message;
}
