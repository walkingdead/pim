package com.zamro.pim.common;


import static java.util.stream.Collectors.toList;

import com.zamro.pim.batch.BatchFailedException;
import com.zamro.pim.exception.CategoryNotFoundException;
import com.zamro.pim.exception.FileDataException;
import com.zamro.pim.exception.ProductNotFoundException;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * Catches all exception and transform them to {@link ErrorEntity} response
 */
@ControllerAdvice
@RestController
public class PImControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(PImControllerAdvice.class);

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity wrongArgumentType(MethodArgumentTypeMismatchException e) {
        LOGGER.error("Wrong arguments: ", e.getMessage());
        return new ErrorEntity(String.format("Incorrect value [%s] for parameter '%s'", e.getValue(), e.getName()));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity missedInputParameters(MissingServletRequestParameterException exception) {
        String errorMessage = String.format("Required [%s] parameter is missing", exception.getParameterName());
        LOGGER.error("Missing parameters ", errorMessage);
        return new ErrorEntity(errorMessage);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity handleResourceNotFoundException(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        StringBuilder builder = new StringBuilder();
        violations.forEach(violation -> builder.append(violation.getMessage()).append("\n"));
        LOGGER.error("Constraint violations ", e.getMessage());
        return new ErrorEntity(builder.substring(0, builder.length() - 1));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity invalidInputParameters(MethodArgumentNotValidException exception) {
        String errorMessage = String.format("Parameter %s %s", exception.getParameter().getParameterName(),
            exception.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(toList()));
        LOGGER.error("Not valid arguments ", errorMessage);
        return new ErrorEntity(errorMessage);
    }

    @ExceptionHandler(ProductNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorEntity handleProductNotFoundException(ProductNotFoundException exception) {
        LOGGER.error("Product not found.", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ExceptionHandler(FileDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity handleFileDataException(FileDataException exception) {
        LOGGER.error("Fail to handle file.", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity handleDataAccessException(CategoryNotFoundException exception) {
        LOGGER.error("Category not found.", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ExceptionHandler(BatchFailedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorEntity handleBatchFailedException(BatchFailedException exception) {
        LOGGER.error(exception.getMessage());
        return new ErrorEntity(exception.getCause().getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity handleRuntimeExceptionException(RuntimeException exception) {
        LOGGER.error(exception.getMessage());
        return new ErrorEntity(exception.getMessage());
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorEntity handleInternalServerError(Throwable exception) {
        LOGGER.error("Unknown server error occurred", exception);
        return new ErrorEntity("Unknown server error");
    }
}