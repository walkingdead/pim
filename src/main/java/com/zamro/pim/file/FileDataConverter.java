package com.zamro.pim.file;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Interface used to describe future contract of file to Java bean conversion.
 * @param <T> target bean type
 */
public interface FileDataConverter<T> {

    /**
     * Converts input stream to collection of java beans
     * @param clazz   target java bean class
     * @param inputStream input stream with data
     * @return list of converted java beans
     * @throws IOException if failed to handle input data
     */
    List<T> convert(Class<T> clazz, InputStream inputStream) throws IOException;
}
