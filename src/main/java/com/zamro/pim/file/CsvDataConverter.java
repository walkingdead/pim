package com.zamro.pim.file;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.CsvToBeanFilter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class for converting csv to java beans
 * @param <T> target bean type
 */
public class CsvDataConverter<T> implements FileDataConverter<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvDataConverter.class);

    /**
     * Converts csv based input stream to target java beans
     * @param clazz   target java bean class
     * @param inputStream input stream with csv base data
     * @return list of converted java beans
     * @throws IOException if failed to read input data
     */
    @Override
    public List<T> convert(Class<T> clazz, InputStream inputStream) throws IOException {
        try (
            Reader reader = new BufferedReader(new InputStreamReader(inputStream));

        ) {
            MinRequiredFields annotation = clazz.getAnnotation(MinRequiredFields.class);
            CsvToBeanFilter filter = line -> line.length >= annotation.count();
            CsvToBean<T> csvToBean = new CsvToBeanBuilder(reader)
                .withType(clazz)
                .withIgnoreLeadingWhiteSpace(true)
                .withFilter(filter)
                .build();
            return csvToBean.parse();
        }
    }
}
